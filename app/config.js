module.exports = {
    parameters: {

    },

    services: {
        "http": {
            path: "/src/http/client/request.js"
        },

        "cache.weather": {
            path: "/src/cache/storage/memory.js",
            arguments: [{
                "key": "weather",
                "ttl": 30000
            }]
        },

        "cache.weather.openweather": {
            path: "/src/cache/storage/memory.js",
            arguments: [{
                "key": "openweather",
                "ttl": 10000
            }]
        },

        "cache.weather.yandex": {
            path: "/src/cache/storage/memory.js",
            arguments: [{
                "key": "yandex",
                "ttl": 10000
            }]
        },

        "weather.controller": {
            path: "/src/weather/controller/v1.js",
            arguments: [
                "@weather.provider",
                {}
            ]
        },

        "weather.provider": {
            path: "/src/weather/provider/composite.js",
            calls: [
                ["add", ["@weather.provider.openweather", "openweather.org"]],
                ["add", ["@weather.provider.yandex", "yandex.ru"]],
                ["injectCache", ["@cache.weather"]]
            ]
        },

        "weather.provider.openweather": {
            path: "/src/weather/provider/openweather.js",
            arguments: [{
                url: "http://api.openweathermap.org/data/2.5/weather"
            }],
            calls: [
                ["injectCache",     ["@cache.weather.openweather"]],
                ["injectHTTP",      ["@http"]],
                ["injectConverter", ["@weather.converter.openweather"]]
            ]
        },

        "weather.provider.yandex": {
            path: "/src/weather/provider/yandex.js",
            arguments: [{
                url: "https://pogoda.yandex.ru/<%= query %>"
            }],
            calls: [
                ["injectCache",     ["@cache.weather.yandex"]],
                ["injectHTTP",      ["@http"]],
                ["injectConverter", ["@weather.converter.yandex"]]
            ]
        },

        "weather.converter.openweather": {
            path: "/src/weather/provider/openweather/converter.js"
        },

        "weather.converter.yandex": {
            path: "/src/weather/provider/yandex/converter.js"
        }
    },

    routes: {
        weather: {
            pattern: "/api/v1/weather/:query",
            method: {
                GET: "@weather.controller:get"
            }
        }
    }
};