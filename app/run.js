var express = require("express"),
    DM      = require("dm"),
    RSVP    = require("rsvp"),
    _       = require("lodash"),
    config  = require("./config"),
    path    = require("path"),
    dm, async, loader, montage,
    app;

// Configure DM
dm = new DM.DependencyManager({
    base: path.resolve(__dirname, "../")
});
async  = new DM.async.RSVP(RSVP);
loader = new DM.loader.cjs(require);
loader.setAsync(async);
dm.setAsync(async).setLoader(loader);

dm.setConfig(config.services, config.paramters);

app = express();

montage = dm.parse(config.routes)
    .then(function(routes) {
        _.forEach(routes, function(definition) {
            var pattern, router, route;

            router = express.Router();

            definition.param && _.forEach(definition.param, function(handler, parameter) {
                router.param(parameter, handler);
            });

            definition.before && definition.before.forEach(function(middleware) {
                router.use(pattern, middleware);
            });

            route = router.route(definition.pattern);

            _.forEach(definition.method, function(controller, method) {
                route[method.toLowerCase()].call(route, controller);
            });

            definition.after && definition.after.forEach(function(middleware) {
                router.use(pattern, middleware);
            });

            // register router in app
            app.use(router);

            console.log("Route %s is mounted", definition.pattern);
        });
    });





module.exports = function(options) {
    options = _.defaults(options || {}, {
        port: 3000
    });

    montage
        .then(function() {
            app.listen(options.port);

            console.log("Listening %d", options.port);
        })
        .catch(function(err) {
            console.log("Could not mount routes", err.stack);
        });
};
