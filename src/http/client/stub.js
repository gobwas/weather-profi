var Client = require("../client"),
    _      = require("lodash"),
    RSVP   = require("rsvp"),
    StubClient;

/**
 * StubClient
 *
 * @class
 * @extends Client
 */
StubClient = Client.extend(
    /**
     * @lends StubClient.prototype
     */
    {
        request: function(url, config) {
            console.log("Requesting %s", url, config);
            return RSVP.Promise.resolve(null);
        }
    }
);

module.exports = StubClient;