var Client  = require("../client"),
    _       = require("lodash"),
    request = require("request"),
    RSVP    = require("rsvp"),
    RequestClient;

/**
 * RequestClient
 *
 * @class
 * @extends Client
 */
RequestClient = Client.extend(
    /**
     * @lends RequestClient.prototype
     */
    {
        request: function(url, config) {
            config = _.defaults(config || {}, {
                method: "GET"
            });

            return new RSVP.Promise(function(resolve, reject) {
                request({
                    url:     url,
                    method:  config.method,
                    qs:      config.query,
                    timeout: 5000
                }, function(err, resp, body) {
                    if (err) {
                        reject(err);
                        return;
                    }

                    resolve(body);
                });
            })
        }
    }
);

module.exports = RequestClient;