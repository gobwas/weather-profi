var inherits = require("inherits-js"),
    _        = require("lodash"),
    Client;

/**
 * @abstract
 * @class Client
 * @constructor
 */
Client = function(options) {
    this.options = _.extend({}, this.constructor.DEFAULTS, options || {});
};

Client.prototype = {
    constructor: Client,

    /**
     * @abstract
     * @param {string} url
     * @param {object} [config]
     */
    request: function(url, config) {
        throw new Error("Method 'request' must be implemented");
    }
};

Client.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Client.DEFAULTS = {};

module.exports = Client;