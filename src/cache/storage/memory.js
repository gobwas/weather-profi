var Storage = require("../storage"),
    _       = require("lodash"),
    RSVP    = require("rsvp"),
    assert  = require("assert"),
    MemoryStorage;

/**
 * MemoryStorage
 *
 * @class
 * @extends Storage
 */
MemoryStorage = Storage.extend(
    /**
     * @lends MemoryStorage.prototype
     */
    {
        constructor: function() {
            Storage.prototype.constructor.apply(this, arguments);

            /**
             *
             * @type {String[]}
             */
            this.keys = [];

            this.expirations = {};
        },

        has: function(key) {
            return RSVP.Promise.resolve(_.has(this.keys, this.createKey(key)));
        },

        find: function(key) {
            return RSVP.Promise.resolve(this.keys[this.createKey(key)] || null);
        },

        get: function(key) {
            var value;

            if (_.isUndefined(value = this.keys[this.createKey(key)])) {
                return RSVP.Promise.reject(new Error("Could not get '" + key + "'"));
            }

            return RSVP.Promise.resolve(value);
        },

        set: function(key, value) {
            var self = this, _key;

            // we do not reject here
            // cause it is not expected usage of #set method
            assert(_.isString(key), "Key expected to be a string");
            assert(!_.isUndefined(value), "Value is expected");

            _key = this.createKey(key);

            this.keys[_key] = value;

            // expire data
            clearTimeout(this.expirations[_key]);
            this.expirations[_key] = setTimeout(function() {
                self.remove(key);
            }, this.options.ttl);

            return RSVP.Promise.resolve();
        },

        remove: function(key) {
            delete this.keys[this.createKey(key)];
            return RSVP.Promise.resolve();
        },

        /**
         * @private
         * @param key
         */
        createKey: function(key) {
            return [this.options.key, key].join(".");
        }
    },

    {
        DEFAULTS: {
            ttl: 1000,
            key: "*"
        }
    }
);

module.exports = MemoryStorage;