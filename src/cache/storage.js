var inherits = require("inherits-js"),
    _        = require("lodash"),
    Storage;

/**
 * @abstract
 * @class Storage
 * @constructor
 */
Storage = function(options) {
    this.options = _.extend({}, this.constructor.DEFAULTS, options || {});
};

Storage.prototype = {
    constructor: Storage,

    /**
     * @abstract
     * @param {string} key
     */
    has: function(key) {
        throw new Error("Method 'has' must be implemented");
    },

    /**
     * @abstract
     * @param {string} key
     */
    find: function(key) {
        throw new Error("Method 'find' must be implemented");
    },

    /**
     * @abstract
     * @param {string} key
     */
    get: function(key) {
        throw new Error("Method 'get' must be implemented");
    },
    
    /**
     * @abstract
     * @param {string} key
     * @param {*} value
     */
    set: function(key, value) {
        throw new Error("Method 'set' must be implemented");
    },
    
    /**
     * @abstract
     * @param {string} key
     */
    remove: function(key) {
        throw new Error("Method 'remove' must be implemented");
    }
};

Storage.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Storage.DEFAULTS = {};

module.exports = Storage;