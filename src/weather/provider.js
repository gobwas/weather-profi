var inherits   = require("inherits-js"),
    _          = require("lodash"),
    Cache      = require("../cache/storage"),
    HTTP       = require("../http/client"),
    Converter  = require("./provider/converter"),
    Criteria   = require("./criteria"),
    murmurhash = require("murmurhash"),
    Provider;

/**
 * @abstract
 * @class Provider
 * @constructor
 */
Provider = function(options) {
    this.options = _.extend({}, this.constructor.DEFAULTS, options || {});
};

Provider.prototype = {
    constructor: Provider,

    injectCache: function(cache) {
        if (!(cache instanceof Cache)) {
            throw new TypeError("Cache is expected");
        }

        if (this.cache) {
            throw new Error("Cache is already set");
        }

        this.cache = cache;
    },

    injectHTTP: function(http) {
        if (!(http instanceof HTTP)) {
            throw new TypeError("HTTP is expected");
        }

        if (this.http) {
            throw new Error("HTTP is already set");
        }

        this.http = http;
    },

    injectConverter: function(converter) {
        if (!(converter instanceof Converter)) {
            throw new TypeError("Converter is expected");
        }

        if (this.converter) {
            throw new Error("Converter is already set");
        }

        this.converter = converter;
    },

    /**
     * @protected
     * @param {Criteria} criteria
     */
    getCacheKey: function(criteria) {
        return murmurhash(JSON.stringify(criteria)).toString();
    },

    /**
     * @abstract
     * @param {Criteria} criteria
     * @returns Promise
     */
    get: function(criteria) {
        throw new Error("Method 'get' must be implemented");
    }
};

Provider.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Provider.DEFAULTS = {

};

module.exports = Provider;