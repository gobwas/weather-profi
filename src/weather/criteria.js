var inherits = require("inherits-js"),
    _        = require("lodash"),
    Criteria;

/**
 * @abstract
 * @class Criteria
 * @constructor
 */
Criteria = function(attributes) {
    _.extend(this, _.pick(attributes, ["query"]));
};

Criteria.prototype = {
    constructor: Criteria
};

Criteria.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Criteria.DEFAULTS = {};

module.exports = Criteria;