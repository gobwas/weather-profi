var inherits = require("inherits-js"),
    _        = require("lodash"),
    Wind;

/**
 * @abstract
 * @class Wind
 * @constructor
 */
Wind = function(attributes) {
    _.extend(this, _.defaults(_.pick(attributes, ["speed", "deg"]), {
        speed: null,
        deg:   null
    }));
};

Wind.prototype = {
    constructor: Wind
};

Wind.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Wind.DEFAULTS = {};

module.exports = Wind;