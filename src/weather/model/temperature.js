var inherits = require("inherits-js"),
    _        = require("lodash"),
    Temperature;

/**
 * @abstract
 * @class Temperature
 * @constructor
 */
Temperature = function(attributes) {
    _.extend(this, _.defaults(_.pick(attributes, ["min", "max"]), {
        min: null,
        max:   null
    }));
};

Temperature.prototype = {
    constructor: Temperature
};

Temperature.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Temperature.DEFAULTS = {};

module.exports = Temperature;