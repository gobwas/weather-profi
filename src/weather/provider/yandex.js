var Provider = require("../provider"),
    _        = require("lodash"),
    Weather  = require("../model"),
    YandexProvider;

/**
 * YandexProvider
 *
 * @class
 * @extends Provider
 */
YandexProvider = Provider.extend(
    /**
     * @lends YandexProvider.prototype
     */
    {
        constructor: function() {
            Provider.prototype.constructor.apply(this, arguments);
            this.urlTemplate = _.template(this.options.url);
        },
        
        get: function(criteria) {
            var self = this,
                cacheKey;

            return this.cache
                .has(cacheKey = this.getCacheKey(criteria))
                .then(function(has) {
                    if (has) {
                        return self.cache
                            .get(cacheKey)
                            .then(function(cached) {
                                return Weather.fromJSON(cached);
                            });
                    }

                    return self.http
                        .request(self.urlTemplate(criteria))
                        .then(function(result) {
                            return self.converter.convert(result);
                        })
                        .then(function(weather) {
                            return self.cache
                                .set(cacheKey, weather)
                                .then(function() {
                                    return weather;
                                });
                        });
                });
        }
    }
);

module.exports = YandexProvider;