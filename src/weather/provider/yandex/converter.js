var Converter   = require("../converter"),
    _           = require("lodash"),
    Weather     = require("../../model"),
    Temperature = require("../../model/temperature"),
    Wind        = require("../../model/wind"),
    jsdom       = require("jsdom"),
    RSVP        = require("rsvp"),
    OpenweatherConverter;

/**
 * OpenweatherConverter
 *
 * @class
 * @extends Converter
 */
OpenweatherConverter = Converter.extend(
    /**
     * @lends OpenweatherConverter.prototype
     */
    {
        convert: function(result) {
            return new RSVP.Promise(function(resolve, reject) {
                var weather;

                jsdom.env(
                    result,
                    [],
                    function (errors, window) {
                        var document, current, temp, tempVal;

                        if (errors) {
                            reject(errors);
                            return;
                        }

                        document = window.document;

                        temp = document.querySelector(".current-weather__thermometer_type_now");
                        tempVal = parseInt(temp.innerHTML.replace(/[^\-\d]/, ""));

                        //result = JSON.parse(result);
                        //
                        weather = new Weather(Date.now());
                        weather.setTemperature(new Temperature({
                            min: tempVal,
                            max: tempVal
                        }));

                        //weather.setWind(new Wind(result.wind));
                        //weather.setPressure(result.main.pressure);

                        resolve(weather);
                    }
                );
            });

        }
    }
);

module.exports = OpenweatherConverter;