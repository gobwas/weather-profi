var Converter   = require("../converter"),
    _           = require("lodash"),
    Weather     = require("../../model"),
    Temperature = require("../../model/temperature"),
    Wind        = require("../../model/wind"),
    RSVP        = require("rsvp"),
    OpenweatherConverter;

/**
 * OpenweatherConverter
 *
 * @class
 * @extends Converter
 */
OpenweatherConverter = Converter.extend(
    /**
     * @lends OpenweatherConverter.prototype
     */
    {
        convert: function(result) {
            return new RSVP.Promise(function(resolve) {
                var weather;

                result = JSON.parse(result);

                weather = new Weather(Date.now());
                weather.setTemperature(new Temperature({
                    min: result.main.temp_min - 273,
                    max: result.main.temp_max - 273
                }));
                weather.setWind(new Wind(result.wind));
                weather.setPressure(result.main.pressure);

                resolve(weather);
            });
        }
    }
);

module.exports = OpenweatherConverter;