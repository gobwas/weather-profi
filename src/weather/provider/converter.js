var inherits = require("inherits-js"),
    _        = require("lodash"),
    Converter;

/**
 * @abstract
 * @class Converter
 * @constructor
 */
Converter = function(options) {
    this.options = _.extend({}, this.constructor.DEFAULTS, options || {});
};

Converter.prototype = {
    constructor: Converter,

    /**
     * @abstract
     */
    convert: function(results) {
        throw new Error("Method 'convert' must be implemented");
    }
};

Converter.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Converter.DEFAULTS = {};

module.exports = Converter;