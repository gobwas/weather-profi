var Provider = require("../provider"),
    _        = require("lodash"),
    Weather  = require("../model"),
    OpenweatherProvider;

/**
 * OpenweatherProvider
 *
 * @class
 * @extends Provider
 */
OpenweatherProvider = Provider.extend(
    /**
     * @lends OpenweatherProvider.prototype
     */
    {
        get: function(criteria) {
            var self = this,
                cacheKey;

            return this.cache
                .has(cacheKey = this.getCacheKey(criteria))
                .then(function(has) {
                    if (has) {
                        return self.cache
                            .get(cacheKey)
                            .then(function(cached) {
                                return Weather.fromJSON(cached);
                            });
                    }

                    return self.http
                        .request(self.options.url, {
                            query: {
                                q: criteria.query
                            }
                        })
                        .then(function(result) {
                            return self.converter.convert(result);
                        })
                        .then(function(weather) {
                            return self.cache
                                .set(cacheKey, weather)
                                .then(function() {
                                    return weather;
                                });
                        });
                });
        }
    }
);

module.exports = OpenweatherProvider;