var Provider    = require("../provider"),
    _           = require("lodash"),
    assert      = require("assert"),
    async       = require("async"),
    Weather     = require("../model"),
    Temperature = require("../model/temperature"),
    Wind        = require("../model/wind"),
    RSVP        = require("rsvp"),
    CompositeProvider;

/**
 * CompositeProvider
 *
 * @class
 * @extends Provider
 */
CompositeProvider = Provider.extend(
    /**
     * @lends CompositeProvider.prototype
     */
    {
        constructor: function(options) {
            Provider.prototype.constructor.apply(this, arguments);

            /**
             * Injected providers.
             * @type {Array}
             */
            this.providers = [];
        },

        add: function(provider, id) {
            assert(provider instanceof Provider, "Provider is expected");
            this.providers.push({provider: provider, id: id});
        },

        get: function(criteria) {
            var self = this,
                cacheKey;

            cacheKey = this.getCacheKey(criteria);

            //return this.cache
            //    .has(cacheKey = this.getCacheKey(criteria))
            //    .then(function(has) {
            //        if (has) {
            //            return self.cache.get(cacheKey);
            //        }

            return new RSVP.Promise(function(resolve, reject) {
                var result;

                result = {};

                async.each(
                    self.providers,
                    function(definition, next) {
                        var provider, id,
                            partialCacheKey;

                        provider = definition.provider;
                        id       = definition.id;

                        partialCacheKey = [cacheKey, id].join(":");

                        provider
                            .get(criteria)
                            .then(function(weather) {
                                self.cache.set(partialCacheKey, weather)
                                    .then(function() {
                                        result[id] = weather;
                                        next(null, weather);
                                    });
                            })
                            .catch(function(err) {
                                // try to fallback
                                return self.cache
                                    .has(partialCacheKey)
                                    .then(function(has) {
                                        if (has) {
                                            return self.cache
                                                .get(partialCacheKey)
                                                .then(function(cached) {
                                                    var weather;

                                                    weather = Weather.fromJSON(cached);

                                                    result[id] = weather;
                                                    next(null, weather);
                                                });
                                        }

                                        throw err;
                                    });
                            })
                            .catch(next);
                    },
                    function(err) {
                        if (err) {
                            reject(err);
                            return;
                        }

                        resolve(result);
                    }
                );
            });
            //});
        }
    }
);

module.exports = CompositeProvider;