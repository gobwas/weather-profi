var inherits = require("inherits-js"),
    _        = require("lodash"),
    Provider = require("./provider"),
    Criteria = require("./criteria"),
    assert   = require("assert"),
    Controller;

/**
 * @abstract
 * @class Controller
 * @constructor
 */
Controller = function(provider, options) {
    assert(provider instanceof Provider, "Provider is expected");
    this.provider = provider;
    this.options = _.extend({}, this.constructor.DEFAULTS, options || {});
};

Controller.prototype = {
    constructor: Controller,
    
    /**
     * @abstract
     */
    get: function(req, res, next) {
        var criteria;

        criteria = new Criteria({
            query: req.params.query
        });

        this.provider
            .get(criteria)
            .then(function(result) {
                res.set(200).send(result);
            })
            //.catch(next)
            .catch(function(err) {
                console.error(err);
                res.set(500).send("Oops");
            });
    }
};

Controller.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Controller.DEFAULTS = {};

module.exports = Controller;