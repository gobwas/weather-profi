var inherits    = require("inherits-js"),
    _           = require("lodash"),
    Temperature = require("./model/temperature"),
    Wind        = require("./model/wind"),
    Weather;

/**
 * @abstract
 * @class Weather
 * @constructor
 */
Weather = function(timestamp, temp, wind, pressure) {
    this.timestamp = timestamp;
    this.temperature = null;
    this.wind        = null;
    this.pressure    = null;
};

Weather.prototype = {
    constructor: Weather,

    setTemperature: function(temperature) {
        this.temperature = temperature;
    },
    setWind: function(wind) {
        this.wind = wind;
    },
    setPressure: function(pressure) {
        this.pressure = pressure;
    }
};

Weather.extend = function(prots, statics) {
    return inherits(this, prots, statics);
};

Weather.DEFAULTS = {};

Weather.fromJSON = function(obj) {
    var weather;

    weather = new Weather(obj.timestamp);
    weather.setTemperature(new Temperature(obj.temperature));
    weather.setWind(new Wind(obj.wind));
    weather.setPressure(obj.pressure);

    return weather;
};

module.exports = Weather;