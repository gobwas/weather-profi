var Controller = require("../controller"),
    _      = require("lodash"),
    V1Controller;

/**
 * V1Controller
 *
 * @class
 * @extends Controller
 */
V1Controller = Controller.extend(
    /**
     * @lends V1Controller.prototype
     */
    {

    }
);

module.exports = V1Controller;